package springboot.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springboot.demo.pojo.Test;
import springboot.demo.service.TestService;

import java.util.List;

@RestController
public class TestController {

    @Autowired
    private TestService testService;


    @RequestMapping("/test/list")
    public List<Test> list() {
        return testService.getList();
    }

    @GetMapping("/{id}")
    public Object getById(@PathVariable("id") Long id) {
        return testService.getById(id);
    }
}

