package springboot.demo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import springboot.demo.mapper.TestMapper;
import springboot.demo.pojo.Demo;
import springboot.demo.pojo.Test;

import java.util.List;

@Service
public class TestService {

    @Autowired
    private TestMapper mapper;

    public List<Test> getList() {
        return mapper.getList();
    }

    public Test getById(Long id){
        return  mapper.getById(id);
    }

}

