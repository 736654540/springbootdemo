package springboot.demo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import springboot.demo.mapper.DemoMapper;
import springboot.demo.mapper.TestMapper;
import springboot.demo.pojo.Test;

import java.util.List;

@Service
public class DemoService {

    @Autowired
    private DemoMapper mapper;

    public List<Test> getList() {
        return mapper.getList();
    }
}

