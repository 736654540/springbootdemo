package springboot.demo.util;

import java.io.Serializable;

public class RequestContext implements Serializable {//打印 WebSocket 日志
    private static ThreadLocal<String> remoteAddr = new ThreadLocal<>();

    public static String getRemoteAddr() {
        return remoteAddr.get();
    }

    public static void setRemoteAddr(String remoteAddr) {
        springboot.demo.util.RequestContext.remoteAddr.set(remoteAddr);
    }
}

