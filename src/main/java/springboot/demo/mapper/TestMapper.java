package springboot.demo.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;
import springboot.demo.pojo.Demo;
import springboot.demo.pojo.Test;

import java.util.List;

@Repository
@Mapper
public interface TestMapper  {
    List<Test> getList();

    Test getById(Long id);
}
