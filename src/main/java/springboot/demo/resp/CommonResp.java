package springboot.demo.resp;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CommonResp<T> {
    /**
     * 返回是否成功
     */
    private boolean isSuccess=true;

    /**
     * 返回失败时，返回的错误信息
     */
    private String message;

    /**
     * 返回成功的时候，返回的内容
     */
    private T content;
}

