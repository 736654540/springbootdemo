import { createApp } from 'vue'
import App from './App.vue'
import router from 'vue-router';
import ElementPlus from 'element-plus';
import 'element-plus/dist/index.css'
import axios from 'axios'





//createApp(App).mount('#app')
const app=createApp(App)
app.use(router).mount('#app')
app.use(ElementPlus)
// 修改axios的baseUrl，这样axios就不用每次都在请求中填写 VUE_APP_SERVER  了
axios.defaults.baseURL=process.env.VUE_APP_SERVER;



