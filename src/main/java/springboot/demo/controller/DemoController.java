package springboot.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springboot.demo.pojo.Test;
import springboot.demo.service.DemoService;
import springboot.demo.service.TestService;

import java.util.List;

@RestController
public class DemoController {

    @Autowired
    private DemoService demoService;


    @RequestMapping("/demo/list")
    public List<Test> list() {
        return demoService.getList();
    }

}

